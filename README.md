# LeBonCoin FizzBuzz API

## Fix
- race condition sur la partie stats
- pas de tests unitaires et pas de tests d'api aux limites
- dirty log
- des panics mal placés
- syntaxe GO particulière

## Run
- `docker-compose up`
  - redis-db
  - go-api
  - python-tests
  
---
### Routes
#### POST /leboncoin/fizzbuzz
- __IN__ (json-payload)
  - Int1  : int
  - Int2  : int
  - Limit : int
  - Str1  : string
  - Str2  : string
- __OUT__ (return)
  - Lstr  : [string]
#### GET /leboncoin/statistics 
- __IN__ (json-payload)
  - None
- __OUT__ (return)
  - Responses = [{ MostUsedRequest, Hints }]
    - *Instead of sending back the most used request, it sends back a list, because when you have two requests with the same hints amount, its better to send both back*
