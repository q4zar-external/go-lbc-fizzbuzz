package main

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"fmt"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"./fizzbuzz"
	"./fizzbuzz/db"

)

func main() {
	// instantiate Echo
	e := echo.New()
	
	// CORS on every routes
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	// middlewares
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())

	// RedisDB
	r := rds.RedisNewClient()
	defer r.Close()
	
	// create routes <api>/leboncoin 
	lbc.RouterFizzBuzz(e, r)
	data, err := json.MarshalIndent(e.Routes(), "", "  ")
	if err != nil {
		fmt.Println(err)
	}

	// write routes into json file
	ioutil.WriteFile("routes.json", data, 0644)

	// run api on port 10000
	e.Logger.Fatal(e.Start(":10000"))
}
