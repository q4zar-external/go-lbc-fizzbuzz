package lbc

import (
    "fmt"
    "net/http"
    "strconv"
    "strings"

    
    "github.com/labstack/echo"
    "github.com/go-redis/redis"

    "./db"
)

// echo router declaration ---------------------------------------------------- |

// echo custom context
type (dbContext struct {
    echo.Context
    db *redis.Client
})

// RouterFizzBuzz ...
// Custom Handler for context in order to add Database connexion inside
func RouterFizzBuzz(e *echo.Echo, r *redis.Client) {
    // allow extracting db from context struct
    e.Use(func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &dbContext{c, r}
			return h(cc)
		}
    })
    // router handlers
    routerLbcFizzBuzz := e.Group("/leboncoin")
    routerLbcFizzBuzz.Match([]string{"POST"}, "/fizzbuzz", handlerFizzBuzz)
    routerLbcFizzBuzz.Match([]string{"GET"}, "/statistics", handlerStatistics)
    routerLbcFizzBuzz.Match([]string{"DELETE"}, "/flushdb", handlerFlushDatabase)
}

// fizzbuzz ------------------------------------------------------------------- |

// FizzBuzzData ...
type FizzBuzzData struct {
    Int1    int             `json:"int1"`
    Int2    int             `json:"int2"`
    Limit   int             `json:"limit"`
    Str1    string          `json:"str1"`
    Str2    string          `json:"str2"`
}

// ResponseFizzBuzz ...
type ResponseFizzBuzz struct {
    Listofstrings   []string  `json:"lstr"`
}

// handlerFizzBuzz ...
func handlerFizzBuzz(c echo.Context) error {
    // context and db ready to use
    cc := c.(*dbContext)
    // extract data from request
    idta := bindQueryParameters(cc)
    // create data for response
    lstr := buildOneToLimit(idta)
    // answer request with response
    resp := ResponseFizzBuzz{
        Listofstrings: lstr,
    }
    return c.JSON(http.StatusCreated, resp)
}

// bindQueryParameters ...
func bindQueryParameters(c *dbContext) *FizzBuzzData { 
    // instantiate struct to receive json parameters
    fbd := new(FizzBuzzData)
    // bind json paramaters to new FizzBuzzData struct
    if err := c.Bind(fbd); err != nil {
        // can't bind properly log and panic 
        println("[ ERROR ] => c.bind(fizzbuzzdata)")
        panic(err)
    }
    // this statement is used if int1 , int2, limit is == 0 aka received as a string
    if (fbd.Int1 == 0 || fbd.Int2 == 0 || fbd.Limit == 0) {
        serr := "Int1 or Int2 or Limit seems to be a string when an int is needed"
        println(serr)
        panic(serr)
    }
    storeQueryParameters(fbd, c.db)
    return fbd
}

// storeQueryParametser ...
func storeQueryParameters(fb *FizzBuzzData, r *redis.Client) {
    // creating a string by concatenating request parameters separated by dot, the value correlating will be the hints counter
    strint := strconv.Itoa(fb.Int1) +"."+ strconv.Itoa(fb.Int2) +"."+ strconv.Itoa(fb.Limit)
    str := strint +"."+ fb.Str1 +"."+ fb.Str2
    rds.StoreRequest(r, str)
}

// buildOneToLimit ...
func buildOneToLimit(reqdata *FizzBuzzData) []string {
    // instantiante array of string
    lstr := []string{""}
    sum := 1
    for sum < reqdata.Limit {
        // multiple of int1 , replace by str1
        if (sum % reqdata.Int1 == 0) {
            lstr = append(lstr, reqdata.Str1)
        // multiple of int2 , replace by str2
        } else if (sum % reqdata.Int2 == 0) {
            lstr = append(lstr, reqdata.Str2)
        // not a multiple convert int to string
        } else {
            lstr = append(lstr, strconv.Itoa(sum))
        }
        sum++
    }
    return lstr
}

// statistics ----------------------------------------------------------------- |

// ResponseStatisticsList ...
type ResponseStatisticsList struct {
    Responses   []ResponseStatistics    `json:"responses"`
}

// ResponseStatistics ...
type ResponseStatistics struct {
    Mostused    FizzBuzzData            `json:"mostused"`
    Hits        int                     `json:"hits"`
}

// handlerStatistics ...
func handlerStatistics(c echo.Context) error {
    // instantiate custom echo context with the db inside
    cc := c.(*dbContext)
    // get maximum hints requests statistics
    max := rds.GetMaxRequest(cc.db)
    // rebuild fizzbuzzdata struct from keystring
    resp := rebuildFizzBuzzDataFromKeyString(max)
    // send response
    return c.JSON(http.StatusCreated, resp)
}

// rebuildFizzBuzzDataFromKeyString ...
func rebuildFizzBuzzDataFromKeyString(keystringlist []string) ResponseStatisticsList {
    fmt.Println(keystringlist)
    // since we can have two answers with the same hints i decided to send back a list of most used requests
    rsp := ResponseStatisticsList{}
    for cnt ,keystring := range keystringlist {
        fmt.Println(cnt)
        // splitting keystring to reproduce fizzbuzzdata origin request
        slice := strings.Split(keystring, ".")
        rp := ResponseStatistics{
            Mostused: FizzBuzzData{
                Int1:   stringToInt(slice[0]),
                Int2:   stringToInt(slice[1]),
                Limit:  stringToInt(slice[2]),
                Str1:   slice[3],
                Str2:   slice[4],
            },
            Hits:    stringToInt(slice[5]),
        }
        // append to responses
        rsp.Responses = append(rsp.Responses, rp)
    }
    return rsp
}

// flushdb -------------------------------------------------------------------- |

func handlerFlushDatabase(c echo.Context) error {
    cc := c.(*dbContext)
    status := cc.db.FlushDB()
    println(status)
    return c.JSON(http.StatusCreated, "OK")
}

// helpers -------------------------------------------------------------------- |

// stringToInt ...
func stringToInt(s string) int {
    // convert string to int
    i, err := strconv.Atoi(s)
    if err != nil {
        println("stringToInt => i")
        panic(err)
    }
    return i
}

