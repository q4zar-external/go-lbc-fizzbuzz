package rds

import (
	"fmt"
	"strconv"

	"github.com/go-redis/redis"
)

// api ------------------------------------------------------------------------ |

// RedisNewClient ...
func RedisNewClient() *redis.Client{
	// instantiate redis new client connexion
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	return client
}

// fizzbuzz ------------------------------------------------------------------ |

// StoreRequest ...
func StoreRequest(redcli *redis.Client, keystring string) {
	// check if key already exists
	val := keyExists(redcli, keystring)
	// don't exists, initialazing key and setting hints counter to 1
	if val == 0 {
		insertKeyVal(redcli, keystring, 1)
	// exists so increment value and insert in base
	} else {
		val++
		insertKeyVal(redcli, keystring, val)
	}
}

// keyExists ...
func keyExists(redcli *redis.Client, keystring string) int {
	// answer true | false if key found in db
	bol, err := redcli.HExists("lbc", keystring).Result()
	if err != nil {
		println("redis.HExists => err != nil")
		panic(err)
	}
	// don't exists send back 0
	if bol == false {
		fmt.Println(keystring, "!exists", bol)
		return 0
	}
	// exists send back hints counter value associated
	fmt.Println(keystring, "exists", bol)
	return getKeyVal(redcli, keystring)
}

// GetKeyVal ...
func getKeyVal(redcli *redis.Client, keystring string) int {
	fmt.Println(keystring)
	val, err := redcli.HGet("lbc", keystring).Result()
	if err != nil {
		println("redis.HGet => error while getting value from key")
		panic(err)
	}
	// convert because the value is a string
	i, err := strconv.Atoi(val)
	return i
}

// insertKeyVal ...
func insertKeyVal(redcli *redis.Client, keystring string, val int) {
	// insert keystring and hints counter
	err := redcli.HSet("lbc", keystring, val).Err()
	if err != nil {
		println("redis.HSet => error while setting key : val")
		panic(err)
	}
}

// statistics ----------------------------------------------------------------- |

// GetMaxRequest ...
func GetMaxRequest(redcli *redis.Client) []string {
	// init var for creating answer
	keystringlist := []string{}
	maxkeystring := ""
	maxcnt := 1
	// get every key associated to lbc
	keys, err := redcli.HGetAll("lbc").Result()
	if err != nil {
		println("redis.HGetAll => error while getting keys from lbc")
		panic(err)
	}
	for keystring, scnt := range keys {
		icnt, err := strconv.Atoi(scnt)
		if err != nil {
			println("strconv.Atoi => error converting")
			panic(err)
		}
		// finding max hints request, compare actual key value to max value from previous entries
		if icnt > maxcnt {
			// actual one is higher so set it as max reference
			maxkeystring = keystring
			maxcnt = icnt
			// append to answer
			keystringlist = []string{maxkeystring + "." + strconv.Itoa(maxcnt)}
		// UPDATE from Initial Instructions
		// so this one is to allow sending back answers imagine that you have two queries with the same 
		// amount of hints, with this you get back both of them
		} else if icnt == maxcnt {
			keystringlist = append(keystringlist, keystring + "." + strconv.Itoa(maxcnt))
		}
	}
	// print and return
	fmt.Println(">>> keystringlist =>", keystringlist)
	return keystringlist
}
