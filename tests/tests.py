import requests
from time import sleep

# ---------------------------------------------------------------------------------

print()
print(f'[ ONE MAX REQUEST ]')

user_1 = {
  "int1":4,
  "int2":7,
  "limit":100,
  "str1":"yo",
  "str2":"dam"
}

user_2 = {
  "int1":4,
  "int2":7,
  "limit":100,
  "str1":"yo",
  "str2":"pao"
}

sleep(0.6)

resp_1 = requests.post(
  url="http://gecko:10000/leboncoin/fizzbuzz",
  json=user_1
)
print(f'!\tstatus-code > [ {resp_1.status_code} ] = json > {resp_1.json()}')

resp_2 = requests.post(
  url="http://gecko:10000/leboncoin/fizzbuzz",
  json=user_2
)
print(f'!\tstatus-code > [ {resp_2.status_code} ] = json > {resp_2.json()}')

resp_2 = requests.post(
  url="http://gecko:10000/leboncoin/fizzbuzz",
  json=user_2
)
print(f'\tstatus-code > [ {resp_2.status_code} ] = json > {resp_2.json()}')

statistics = requests.get(url="http://gecko:10000/leboncoin/statistics",)
print(f'!\tstatus-code > [ {statistics.status_code} ] = json > {statistics.json()}')

# ---------------------------------------------------------------------------------

sleep(0.6)
print()
print(f'[ FLUSHDB ]')
flushdb = requests.delete(url="http://gecko:10000/leboncoin/flushdb",)
print(f'!\tstatus-code > [ {flushdb.status_code} ] = json > {flushdb.json()}')

# ---------------------------------------------------------------------------------

sleep(0.6)
print()
print(f'[ TWO MAX REQUEST ]')

user_1 = {
  "int1":4,
  "int2":7,
  "limit":100,
  "str1":"yo",
  "str2":"dam"
}

user_2 = {
  "int1":4,
  "int2":7,
  "limit":100,
  "str1":"yo",
  "str2":"pao"
}

sleep(0.6)

resp_1 = requests.post(
  url="http://gecko:10000/leboncoin/fizzbuzz",
  json=user_1
)
print(f'!\tstatus-code > [ {resp_1.status_code} ] = json > {resp_1.json()}')

resp_2 = requests.post(
  url="http://gecko:10000/leboncoin/fizzbuzz",
  json=user_2
)
print(f'!\tstatus-code > [ {resp_2.status_code} ] = json > {resp_2.json()}')

statistics = requests.get(url="http://gecko:10000/leboncoin/statistics",)
print(f'!\tstatus-code > [ {statistics.status_code} ] = json > {statistics.json()}')


# ---------------------------------------------------------------------------------